# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Rivendell LibRadio"
HOMEPAGE="http://www.rivendellaudio.org"
SRC_URI="http://www.rivendellaudio.org/ftpdocs/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="sys-kernel/gpio
	=x11-libs/qt-3*"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/sandbox.patch
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
}
