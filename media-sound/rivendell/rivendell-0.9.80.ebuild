# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Rivendell Open Source Radio Automation"
HOMEPAGE="http://www.rivendellaudio.org"
SRC_URI="http://www.rivendellaudio.org/ftpdocs/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="media-libs/libradio
	=x11-libs/qt-3*"
RDEPEND="${DEPEND}"

pkg_setup() {
	enewgroup rivendell
	enewuser rivendell -1 /bin/bash /var/lib/rivendell rivendell
}

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/sandbox.patch
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"

	insinto /etc
	doins conf/rd.conf-sample

	mkdir ${D}/var/snd
	chown rivendell:rivendell ${D}/var/snd
}
