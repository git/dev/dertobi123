# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-apps/cpqarrayd/cpqarrayd-2.2.ebuild,v 1.1 2006/04/01 09:14:07 dertobi123 Exp $

DESCRIPTION="Compaq Arraycontroller Daemon"
HOMEPAGE="http://www.strocamp.net/opensource/cpqarrayd.php"
SRC_URI="http://www.strocamp.net/opensource/compaq/downloads/${P}.tar.gz"

IUSE="snmp"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="snmp? ( net-analyzer/net-snmp )"
RDEPEND="${DEPEND}"

src_compile() {
	econf \
		$(use_enable snmp snmptrap ) \
		|| die "Configure failed"

	emake || die "Make failed"
}

src_install() {
	einstall

	dodoc NEWS AUTHORS README

	newinitd "${FILESDIR}"/cpqarrayd.init cpqarrayd
	newconfd "${FILESDIR}"/cpqarrayd.confd cpqarrayd
}
