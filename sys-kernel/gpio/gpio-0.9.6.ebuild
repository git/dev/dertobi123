# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-cluster/gfs-kernel/gfs-kernel-1.04.00.ebuild,v 1.2 2007/05/12 13:28:14 xmerlin Exp $

inherit eutils linux-mod linux-info

DESCRIPTION="MeasurementComputing GPIO Driver"
HOMEPAGE="http://www.rivendellaudio.org"
SRC_URI="http://www.rivendellaudio.org/ftpdocs/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

IUSE=""

DEPEND=">=virtual/linux-sources-2.6.17"

RDEPEND=""

pkg_setup() {
	linux-mod_pkg_setup
	case ${KV_FULL} in
		2.2.*|2.4.*) die "${P} supports only 2.6 kernels";;
	esac
}

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/sandbox.patch
}

src_compile() {
	set_arch_to_kernel

	econf || die "configure problem"
	emake || die "compile problem"
}

src_install() {
	emake DESTDIR=${D} install || die "install problem"
}

pkg_postinst() {
	linux-mod_pkg_postinst

	einfo ""
	einfo "Please remember to re-emerge ${PN} when you upgrade your kernel!"
	einfo ""
}
